$(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
      $('#animation').addClass('changeColor01')
    } else {
      $('#animation').removeClass('changeColor01')
    }
    if ($(this).scrollTop() > 500) {
      $('#animation').addClass('changeColor02')
    } else {
      $('#animation').removeClass('changeColor02')
    }
    if ($(this).scrollTop() > 1700) {
      $('#animation').addClass('changeColor03')
    } else {
      $('#animation').removeClass('changeColor03')
    }
  })
})

/******************************************************
 * 
 * Script pour la fenêtre modal
 ******************************************************/
 let modalBtn = document.getElementById("modal-btn")
 let modal = document.querySelector(".modal")
 let closeBtn = document.querySelector(".close-btn")
 let button = document.getElementById('btn-submit')
 modalBtn.onclick = function(){
   modal.style.display = "block"
 }
 closeBtn.onclick = function(){
   modal.style.display = "none"
 }
 window.onclick = function(e){
   if(e.target == modal){
     modal.style.display = "none"
   }
 }
button.onclick = function(){

 modal.style.display="none"
}

/****************************
 * animation du txt
 *****************************/
const txtAnim = document.querySelector('.positionh1')
let typewriter = new Typewriter(txtAnim, {
  loop: false,
  deleteSpeed:20
})

typewriter
.pauseFor(1800)
.changeDelay(20)
.typeString('Moi c\'est Nabil Bouazzaoui')
.pauseFor(300)
.typeString('<strong>, Développeur Full-Stack</strong>')
.pauseFor(1000)
.deleteChars(11)
.typeString('<span style="color: #27ae60;"> HTML</span>')
.pauseFor(1000)
.deleteChars(5)
.typeString('<span style="color: #EA39ff;"> CSS</span>')
.pauseFor(1000)
.deleteChars(5)
.typeString('<span style="color: midnightblue;"> JavaScript</span>')
.pauseFor(1000)
.deleteChars(10)
.typeString('<span style="color: #ff6910;"> NodeJs</span>')
.pauseFor(1000)
.deleteChars(6)
.typeString('<span style="color: #002554;"> Web</span>')
.start()